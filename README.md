1.5: One Away
======================================

### STATUS:
* master: [![pipeline status](https://gitlab.com/cracking-the-coding-interview/one-away/badges/master/pipeline.svg)](https://gitlab.com/cracking-the-coding-interview/one-away/commits/master)
* develop: [![pipeline status](https://gitlab.com/cracking-the-coding-interview/one-away/badges/develop/pipeline.svg)](https://gitlab.com/cracking-the-coding-interview/one-away/commits/develop)

======================================
### Table of Contents
1. [Running Tests](https://gitlab.com/cracking-the-coding-interview/one-away#running-tests)
2. [Problem Statement](https://gitlab.com/cracking-the-coding-interview/one-away#problem-statement)
3. [Questions](https://gitlab.com/cracking-the-coding-interview/one-away#questions)
4. [Solution](https://gitlab.com/cracking-the-coding-interview/one-away#solution)
5. [Implementation](https://gitlab.com/cracking-the-coding-interview/one-away#implementation)
6. [Additional notes](https://gitlab.com/cracking-the-coding-interview/one-away#additional-notes)
7. [References](https://gitlab.com/cracking-the-coding-interview/one-away#running-tests)

# Running Tests:
I'm using Gulp as a task runner, eslint
(with airbnb coding  standards) for code linting,
and mocha as a testing framework.  To run tests, after a fresh
clone of this repository, run:
```
npm install
npm test
```
The grunt test task will run the lint and mocha tasks sequentially.  Those
tasks will run eslint, and mocha.

# Problem Statement:
There are three types of edits that can be performed on strings:
  1. Insert a character.
  2. Remove a character.
  3. Replace a character.

Given two strings, write a function to check if they are one edit (or zero
edits)
away.

  Example:
  pale, ple -> true
  pales, pale -> true
  pale, bale -> true
  pale, bake -> false

# Questions:
  1. Will the input strings have leading/trailing white space?  Will they
  need to be trimmed?

  2. Is it correct to assume that strings could have internal white space,
  and be considered?  The example strings don't have internal white space,
  but I wanted to be clear if strings with internal white space would be
  valid inputs.

  3. I'm assuming every change to the same character (inserts, removes,
  replaces) count as separate edits.  Is this correct?

  4. Should this solution be case sensitive?

  5. Will these strings include unicode characters?  Astral plane
   characters?  Combining marks?

  6. The problem says "one edit away", but the problem statement says
  "or zero edits away".  If these strings are zero edits away, should the
  code return true?  Or are we strictly looking for strings that are "one
  edit away"?

# Solution:
If the strings are exactly the same, they are zero edits away.  This can be
checked first.

If the strings are not equal, but their lengths are equal, or different
by 1, they can still be one edit away.  But if their lengths are
different by 2 or more, they cannot be one edit away.

As we iterate through the two strings, once we find the pair of characters
that are different:

It turns out that irregardless of what edit we make (obviously we'll make
the edit that makes the strings closer to equal) the rest of the
strings have to be the same for them to be one edit away.

So we don't actually need to do the edit, we just have to check the
substrings (determined by the type of edit) that need to be equal for
the strings to be one edit away.

substrings to check:

Insert:
	substring(str1, index + 1, len - 1) === substring(str2, index, len - 1)

Replace:
	substring(str1, index + 1, len - 1) === substring(str2, index + 1, len - 1)

Remove
	substring(str1, index, len - 1) === substring(str2, index + 1, len -1)

# Implementation:
If either of the input strings are null, throw an exception.

If either of the input strings are not strings, throw an exception.

If both input strings are empty, they are zero edits away.  Return true.

If both input strings are equal, they are zero edits away.  Return true.

Check length of the input strings.  If different by 2 or more they are not
one edit away.  Return false.

Iterate through both input strings linearly.

As we go through the input strings, for each pair of characters, we check to
see if they are equal.  If they are not equal, we check the boolean
substring checks for each of the three above cases (insert, replace, and
remove).  If any of these boolean expressions evaluate to true, the strings
are one edit away.  If none of them are true, the strings are not one edit away.

# Additional notes:

# References:
- JavaScript String API documentation.
- JavaScript Array API documentation.
