class ArrayString {
  constructor(str) {
    this.data = Array.from(str);
  }

  /*
   * Given an index, return the character at that position.
   *
   * Throws an exception if index < 0, or index >= data.length
   */
  getCharAt(index) {
    if (typeof index !== 'number') {
      throw new Error('index is not a number.');
    }

    if (index < 0) {
      throw new Error('index is negative.');
    }

    if (index >= this.data.length) {
      throw new Error('index is out of bounds');
    }

    return this.data[index];
  }

  /*
   * Given ArrayString other, is this ArrayString equal to other?
   *
   * Throws an exception if other is not an ArrayString.
   * Throws an exception if other is null.
   */
  equals(other) {
    if (other === null) {
      throw new Error('other is null.');
    }

    if (other.constructor.name !== 'ArrayString') {
      throw new Error('other is not an ArrayString.');
    }

    if (this.data.length !== other.length()) {
      return false;
    }

    for (let index = 0; index < this.data.length; index += 1) {
      if (this.data[index] !== other.getCharAt(index)) {
        return false;
      }
    }
    return true;
  }

  /*
   * Given two numbers start, and end, returns the Array slice from
   * start to end - 1. Array[start:end - 1]
   *
   * start is required.
   * end is optional. (If there is no end passed in, assume end should be data.length)
   *
   * Throws an exception if start or end are not numbers.
   * Throws an exception if start < 0 or end < 0.
   * Throws an exception if start < end.
   * Throws an exception if start or end are equal to or greater than
   * data.length.
   */
  getSubString(start, end) {
    if (typeof start !== 'number' || (end !== undefined && typeof end !== 'number')) {
      throw new Error('start and/or end are not a numbers.');
    }

    if (start < 0 || (end !== undefined && end < 0)) {
      throw new Error('start and/or end are negative.');
    }

    if (end !== undefined && start > end) {
      throw new Error('start cannot be greater than end.');
    }

    if (start > this.data.length) {
      throw new Error('start is out of bounds.');
    }

    if (end !== undefined && end > this.data.length) {
      throw new Error('end is out of bounds.');
    }

    if (end === undefined) {
      return this.data.slice(start, this.data.length);
    }
    return this.data.slice(start, end);
  }

  /*
   * Returns data.length
   */
  length() {
    return this.data.length;
  }
}

module.exports = ArrayString;
