const assert = require('assert');
const ArrayString = require('../src/array-string');

const helpers = {
  areArraysEqual(array1, array2) {
    if (!Array.isArray(array1) || !Array.isArray(array2)) {
      throw new Error('inputs are not arrays.');
    }

    if (array1.length !== array2.length) {
      return false;
    }

    for (let index = 0; index < array1.length; index += 1) {
      if (array1[index] !== array2[index]) {
        return false;
      }
    }
    return true;
  },
};

describe('ArrayString tests:', () => {
  describe('getCharAt():', () => {
    it('should throw exception when index is not a number.', () => {
      const arrayString = new ArrayString('Foo Bar');
      assert.throws(() => {
        arrayString.getCharAt('Foo Bar');
      }, Error, 'index is not a number');
    });

    it('should throw exception when index is negative.', () => {
      const arrayString = new ArrayString('Foo Bar');
      assert.throws(() => {
        arrayString.getCharAt(-1);
      }, Error, 'index is negative.');
    });

    it('should throw exception when index is positive, but out of bounds.', () => {
      const arrayString = new ArrayString('Foo Bar');
      assert.throws(() => {
        arrayString.getCharAt(100);
      }, Error, 'index is out of bounds');
    });

    it('should return correct result for valid index. (non-astral plane char)', () => {
      const arrayString = new ArrayString('Foo Bar');
      assert.equal(arrayString.getCharAt(0), 'F');
    });

    it('should return correct result for valid index. (astral plane char)', () => {
      const arrayString = new ArrayString('Iñtërnâtiônàlizætiøn☃💩');
      assert.equal(arrayString.getCharAt(3), 'ë');
    });
  });

  describe('equals():', () => {
    it('should throw exception when other is not an ArrayString', () => {
      const arrayString = new ArrayString('Foo Bar');
      assert.throws(() => {
        arrayString.equals('Not an ArrayString, haha!');
      }, Error, 'other is not an ArrayString');
    });

    it('should throw exception when other is null', () => {
      const arrayString = new ArrayString('Foo Bar');
      assert.throws(() => {
        arrayString.equals(null);
      }, Error, 'other is null');
    });

    it('should return false when the strings are not of equal length. (no astral plane chars)', () => {
      const arrayString = new ArrayString('Foo Bar');
      const other = new ArrayString('Foo');
      assert.equal(arrayString.equals(other), false);
    });

    it('should return false when the strings are not of equal length. (astral plane chars)', () => {
      const arrayString = new ArrayString('Iñtërnâtiônàlizætiøn☃ð');
      const other = new ArrayString('Iñtërlizætiøn☃ð');
      assert.equal(arrayString.equals(other), false);
    });

    it('should return false when the strings are equal length, but not equal. (no astral plane chars)', () => {
      const arrayString = new ArrayString('Foo Bar');
      const other = new ArrayString('Fxx Bar');
      assert.equal(arrayString.equals(other), false);
    });

    it('should return false when the strings are equal length, but not equal. (astral plane chars)', () => {
      const arrayString = new ArrayString('Iñtërnâtiônàlizætiøn☃ð');
      const other = new ArrayString('IñtërnâtiôAàlizætiøn☃ð');
      assert.equal(arrayString.equals(other), false);
    });

    it('should return true when the strings are equal. (no astral plane chars)', () => {
      const arrayString = new ArrayString('Foo Bar');
      const other = new ArrayString('Foo Bar');
      assert.equal(arrayString.equals(other), true);
    });

    it('should return true when the strings are equal. (astral plane chars)', () => {
      const arrayString = new ArrayString('Iñtërnâtiônàlizætiøn☃ð');
      const other = new ArrayString('Iñtërnâtiônàlizætiøn☃ð');
      assert.equal(arrayString.equals(other), true);
    });

    it('should return true when the strings are equal. (empty strings)', () => {
      const arrayString = new ArrayString('');
      const other = new ArrayString('');
      assert.equal(arrayString.equals(other), true);
    });
  });

  describe('getSubString():', () => {
    it('should throw exception when start is not a number.', () => {
      const str = 'Foo Bar';
      const arrayString = new ArrayString(str);
      assert.throws(() => {
        arrayString.getSubString('NaNa', 3);
      }, Error, 'start and/or end are not a numbers.');
    });

    it('should throw exception when end is not a number.', () => {
      const str = 'Foo Bar';
      const arrayString = new ArrayString(str);
      assert.throws(() => {
        arrayString.getSubString(0, 'NaNa');
      }, Error, 'start and/or end are not a numbers.');
    });

    it('should throw exception when start and end are not numbers.', () => {
      const str = 'Foo Bar';
      const arrayString = new ArrayString(str);
      assert.throws(() => {
        arrayString.getSubString('NaNa', 'NaNa');
      }, Error, 'start and/or end are not a numbers.');
    });

    it('should throw exception when start is negative', () => {
      const str = 'Foo Bar';
      const arrayString = new ArrayString(str);
      assert.throws(() => {
        arrayString.getSubString(-1, 3);
      }, Error, 'start and/or end are negative.');
    });

    it('should throw exception when end is negative', () => {
      const str = 'Foo Bar';
      const arrayString = new ArrayString(str);
      assert.throws(() => {
        arrayString.getSubString(-9, -1);
      }, Error, 'start and/or end are negative.');
    });

    it('should throw exception when start > end.', () => {
      const str = 'Foo Bar';
      const arrayString = new ArrayString(str);
      assert.throws(() => {
        arrayString.getSubString(3, 2);
      }, Error, 'start cannot be greater than end.');
    });

    it('should throw exception when start > data.length.', () => {
      const str = 'Foo Bar';
      const arrayString = new ArrayString(str);
      assert.throws(() => {
        arrayString.getSubString(100, 200);
      }, Error, 'start is out of bounds.');
    });

    it('should throw exception when end > data.length.', () => {
      const str = 'Foo Bar';
      const arrayString = new ArrayString(str);
      assert.throws(() => {
        arrayString.getSubString(1, 200);
      }, Error, 'end is out of bounds.');
    });

    it('should return correct result for valid start and end. (no astral plane chars)', () => {
      const str = 'Foo Bar';
      const arrayString = new ArrayString(str);
      const start = 1;
      const end = 4;
      const expected = Array.from(str.substring(start, end));
      const actual = arrayString.getSubString(start, end);

      assert.equal(helpers.areArraysEqual(actual, expected), true);
    });

    it('should return correct result for valid start and end. (astral plane chars)', () => {
      const str = 'Iñtërnâtiônàlizætiøn☃ð';
      const arrayString = new ArrayString(str);
      const start = 1;
      const end = 4;
      const expected = Array.from(str.substring(start, end));
      const actual = arrayString.getSubString(start, end);

      assert.equal(helpers.areArraysEqual(actual, expected), true);
    });

    it('should return correct result for valid start, and no end. (no astral plane chars)', () => {
      const str = 'Foo Bar';
      const arrayString = new ArrayString(str);
      const start = 1;
      const expected = Array.from(str.substring(start));
      const actual = arrayString.getSubString(start);

      assert.equal(helpers.areArraysEqual(actual, expected), true);
    });

    it('should return correct result for valid start, and no end. (astral plane chars)', () => {
      const str = 'Iñtërnâtiônàlizætiøn☃ð';
      const arrayString = new ArrayString(str);
      const start = 1;
      const expected = Array.from(str.substring(start));
      const actual = arrayString.getSubString(start);

      assert.equal(helpers.areArraysEqual(actual, expected), true);
    });
  });

  describe('length():', () => {
    it('should return correct result. (empty string)', () => {
      const arrayString = new ArrayString('');
      assert.equal(arrayString.length(), 0);
    });

    it('should return correct result. (no astral plane chars)', () => {
      const str = 'Foo Bar';
      const arrayString = new ArrayString(str);
      assert.equal(arrayString.length(), Array.from(str).length);
    });

    it('should return correct result. (astral plane chars)', () => {
      const str = 'Iñtërnâtiônàlizætiøn☃ð';
      const arrayString = new ArrayString(str);
      assert.equal(arrayString.length(), Array.from(str).length);
    });
  });
});
