const assert = require('assert');
const oneAwayModule = require('../src/one-away');

describe('One Away Tests:', () => {
  describe('isOneAway():', () => {
    it('should throw exception if str1 and/or str2 are null', () => {
      assert.throws(() => {
        oneAwayModule.isOneAway(null, null);
      }, Error, 'str1 and/or str2 are null.');
    });

    it('should throw exception if str1 and/or str2 are not strings', () => {
      assert.throws(() => {
        oneAwayModule.isOneAway(4, 5);
      }, Error, 'str1 and/or str2 not strings.');
    });

    it('should return false if str1 and str2 lengths are different by 2 or more characters', () => {
      const str1 = 'Foo Bar';
      const str2 = 'Foo';
      const expected = false;
      const actual = oneAwayModule.isOneAway(str1, str2);

      assert.equal(actual, expected);
    });

    it('should return false if input strings are not one edit away. (no astral plane chars)', () => {
      const str1 = 'Foo Bar';
      const str2 = 'FoO Bxi';
      const expected = false;
      const actual = oneAwayModule.isOneAway(str1, str2);

      assert.equal(actual, expected);
    });

    it('should return false if input strings are not one edit away. (astral plane chars)', () => {
      const str1 = 'Iñtërnâtiônàli3ætiøn☃💩';
      const str2 = 'IñtërXâtiônàMizætiøn☃💩';
      const expected = false;
      const actual = oneAwayModule.isOneAway(str1, str2);

      assert.equal(actual, expected);
    });

    it('should return true if both strings are empty strings. (zero edits away)', () => {
      const str1 = '';
      const str2 = '';
      const expected = true;
      const actual = oneAwayModule.isOneAway(str1, str2);

      assert.equal(actual, expected);
    });

    it('should return true if both strings are equal. (no astral chars)', () => {
      const str1 = 'Foo Bar';
      const str2 = 'Foo Bar';
      const expected = true;
      const actual = oneAwayModule.isOneAway(str1, str2);

      assert.equal(actual, expected);
    });

    it('should return true if both strings are equal. (astral chars)', () => {
      const str1 = 'Iñtërnâtiônàlizætiøn☃💩';
      const str2 = 'Iñtërnâtiônàlizætiøn☃💩';
      const expected = true;
      const actual = oneAwayModule.isOneAway(str1, str2);

      assert.equal(actual, expected);
    });

    it('should return true if input strings are one edit away. (replace case)(no astral chars)', () => {
      const str1 = 'Foo Bar';
      const str2 = 'FoX Bar';
      const expected = true;
      const actual = oneAwayModule.isOneAway(str1, str2);

      assert.equal(actual, expected);
    });

    it('should return true if input strings are one edit away. (replace case)(astral plane chars)', () => {
      const str1 = 'Iñtërnâtiônàlizætiøn☃💩';
      const str2 = 'IñtërnâtiônXlizætiøn☃💩';
      const expected = true;
      const actual = oneAwayModule.isOneAway(str1, str2);

      assert.equal(actual, expected);
    });

    it('should return true if input strings are one edit away. (insert case)(no astral chars)', () => {
      const str1 = 'Foo Bar';
      const str2 = 'Fo Bar';
      const expected = true;
      const actual = oneAwayModule.isOneAway(str1, str2);

      assert.equal(actual, expected);
    });

    it('should return true if input strings are one edit away. (insert case)(astral plane chars)', () => {
      const str1 = 'Iñtërnâtiônàlizætiøn☃💩';
      const str2 = 'Iñtërnâtônàlizætiøn☃💩';
      const expected = true;
      const actual = oneAwayModule.isOneAway(str1, str2);

      assert.equal(actual, expected);
    });

    it('should return true if input strings are one edit away. (remove case)(no astral chars)', () => {
      const str1 = 'Foo Bar';
      const str2 = 'FooX Bar';
      const expected = true;
      const actual = oneAwayModule.isOneAway(str1, str2);

      assert.equal(actual, expected);
    });

    it('should return true if input strings are one edit away. (remove case)(astral plane chars)', () => {
      const str1 = 'Iñtërnâtiônàlizætiøn☃💩';
      const str2 = 'IñtërnâtiônCàlizætiøn☃💩';
      const expected = true;
      const actual = oneAwayModule.isOneAway(str1, str2);

      assert.equal(actual, expected);
    });

    it('should return true if input strings are one edit away. (remove case, last char)(no astral chars', () => {
      const str1 = 'Foo Bar';
      const str2 = 'Foo BarX';
      const expected = true;
      const actual = oneAwayModule.isOneAway(str1, str2);

      assert.equal(actual, expected);
    });

    it('should return true if both strings are one edit away. (remove case, last char)(astral chars)', () => {
      const str1 = 'Iñtërnâtiônàlizætiøn☃💩';
      const str2 = 'Iñtërnâtiônàlizætiøn☃💩X';
      const expected = true;
      const actual = oneAwayModule.isOneAway(str1, str2);

      assert.equal(actual, expected);
    });
  });

  describe('areArraysEqual():', () => {
    it('should throw exception when array1 and/or array2 are null', () => {
      assert.throws(() => {
        oneAwayModule.areArraysEqual(null, null);
      }, Error, 'array1 and/or array2 are null.');
    });

    it('should throw exception when array1 and/or array2 are not arrays', () => {
      assert.throws(() => {
        oneAwayModule.areArraysEqual(5, 6);
      }, Error, 'array1 and/or array2 are not arrays.');
    });

    it('should return true if array1 and array2 are empty', () => {
      const array1 = [];
      const array2 = [];
      const expected = true;
      const actual = oneAwayModule.areArraysEqual(array1, array2);

      assert.equal(actual, expected);
    });

    it('should return false if array1.length and array2.lengths are not equal', () => {
      const array1 = [3, 4, 5];
      const array2 = [5, 4];
      const expected = false;
      const actual = oneAwayModule.areArraysEqual(array1, array2);

      assert.equal(actual, expected);
    });

    it('should return false if array1 and array2 are not equal. (no astral plane chars)', () => {
      const array1 = [3, 4, 5];
      const array2 = [5, 4, 3];
      const expected = false;
      const actual = oneAwayModule.areArraysEqual(array1, array2);

      assert.equal(actual, expected);
    });

    it('should return false if array1 and array2 are not equal. (astral plane chars)', () => {
      const array1 =
      [
        'I',
        'ñ',
        't',
        'ë',
        'r',
        'n',
        'â',
        't',
        'i',
        'ô',
        'n',
        'à',
        'l',
        'i',
        'z',
        'æ',
        't',
        'i',
        'ø',
        'n',
        '☃',
        '💩',
      ];
      const array2 =
      [
        'I',
        'ñ',
        't',
        'ë',
        'r',
        'n',
        'â',
        'i',
        'ô',
        'n',
        'à',
        'l',
        'i',
        'z',
        'æ',
        't',
        'i',
        'ø',
        'n',
        '☃',
        '💩',
      ];
      const expected = false;
      const actual = oneAwayModule.areArraysEqual(array1, array2);

      assert.equal(actual, expected);
    });

    it('should return true if array1 and array2 are equal. (no astral plane chars)', () => {
      const array1 = ['F', 'o', 'o', ' ', 'B', 'a', 'r'];
      const array2 = ['F', 'o', 'o', ' ', 'B', 'a', 'r'];
      const expected = true;
      const actual = oneAwayModule.areArraysEqual(array1, array2);

      assert.equal(actual, expected);
    });

    it('should return true if array1 and array2 are equal. (astral plane chars)', () => {
      const array1 =
      [
        'I',
        'ñ',
        't',
        'ë',
        'r',
        'n',
        'â',
        'i',
        'ô',
        'n',
        'à',
        'l',
        'i',
        'z',
        'æ',
        't',
        'i',
        'ø',
        'n',
        '☃',
        '💩',
      ];
      const array2 =
      [
        'I',
        'ñ',
        't',
        'ë',
        'r',
        'n',
        'â',
        'i',
        'ô',
        'n',
        'à',
        'l',
        'i',
        'z',
        'æ',
        't',
        'i',
        'ø',
        'n',
        '☃',
        '💩',
      ];
      const expected = true;
      const actual = oneAwayModule.areArraysEqual(array1, array2);

      assert.equal(actual, expected);
    });
  });
});
